---
title: Do Código a Produção
subtitle: workshop de pipelines com Gitlab CI/CD, Docker e AWS
author: Edson/Cassiano
date: 9 de Abril de 2024 - https://secs2024.edsoncelio.dev
title-slide-attributes:
    data-background-size: "auto 10%, auto 12%"
    data-background-position: "95% 95%, 70% 97%"
---

# Quem somos

## Edson

:::incremental

- Site Reliability Engineer na Yampi
- Graduado em ADS (Anhanguera) e Pós Graduando em Arquitetura de Soluções (PUCMinas)
- Mantenedor da documentação do Kubernetes (em PTBR) e do Glossário da CNCF (em PTBR)
- Membro do programa AWS Community Builders e Grafana Champions
- Contatos: /in/edsoncelio, edsoncelio.dev, @tuxpilgrim

:::

## Cassiano

:::incremental

- Suporte Operacional na Prefeitura Municipal de Sobral - PMS
- Graduando em Engenharia de Computação pela Universidade Federal do Ceará - UFC
- Contatos: /in/souzacassiano/, https://github.com/ciano123

:::

# Avisos

:::incremental

- É esperado conhecimento básico em Git
- É esperado conhecimento básico de sistemas operacionais
- É esperado conhecimento básico de computação em nuvem

:::


# Agenda

:::incremental

- Conceitos de pipelines de CI/CD
- Conceitos de containers e Docker
- Conceitos de AWS
- Gitlab/Gitlab CI/CD
- Parte Prática
- Outras Ferramentas de Mercado
- Dúvidas e Encerramento

:::

# Conceitos de pipelines de CI/CD

## Antes de tudo, o que é um pipeline?
![pipeline](images/pipeline.jpg){ style="width: 68%; margin: auto;" }

---
No contexto de DevOps, pipeline é uma série de etapas a serem realizadas para disponibilizar 
uma nova versão de um software em produção.

## Pipelines de CI/CD
* CI: *Continuous Integration* (Integração Contínua)
* CD: *Continuous Delivery* (Entrega Contínua) e *Continous Deployment* (Implantação Contínua)

## *Continuous Integration* (Integração Contínua)
Integração Contínua é uma prática de desenvolvimento onde desenvolvedores mesclam as suas mudanças de código em um repositório 
central regularmente, após execução automatizada de construção do código e testes.

## *Continuous Delivery* (Entrega Contínua)
A entrega contínua é uma extensão da integração contínua, uma vez que implementa de modo automático todas as alterações de código em um ambiente de teste e/ou produção após o estágio de build.

## *Continuous Deployment* (Implantação Contínua)
A implantação contínua vai um passo além da entrega contínua. Com essa prática, toda alteração que passa em todos os estágios do seu pipeline de produção é lançada para os clientes. Não há intervenção humana, e apenas um teste com falha vai impedir a implementação de uma nova alteração na produção.

## Fluxo completo
![fluxo de CI/CD](images/ci-cd.png){ width=100% }

# Conceitos de containers e Docker

## O que é um containers?
Os *Containers* nada mais são do que uma abstração a nível de Sistema Operacional, que se caracterizam por agrupar código, bibliotecas e dependências a fim de executar aplicações em ambientes isolados.

![What is container](images/what-is-container.png){ width=85% }

## Virtualização vs Containerização

![Virtualização vs Containerização](images/virtual_vs_container.png){ width=95% }

## Docker

Docker é uma plataforma de código aberto que permite construir, implantar, executar, atualizar e gerenciar *Conteineres*.

![Docker](images/docker.png){ width=100% }

## Por que usar o Docker?

* Portabilidade aprimorada e contínua de contêineres
* Atualizações ainda mais leves e mais granulares
* Criação automatizada de contêineres
* Controle de versão de contêiner
* Reutilização de contêineres

## Ferramentas e termos do Docker

* Dockerfile - Automatiza o processo de criação de imagens *Docker*

```yaml
FROM golang:1.22.1

# Download Go modules
COPY go.mod go.sum ./
RUN go mod download
# [...]
# Expoe na porta 8080
EXPOSE 8080

# Run
CMD ["./hello-world"]

```

[referências sobre o Dockerfile](https://docs.docker.com/reference/dockerfile/)

## Ferramentas e termos do Docker

* Imagens Docker - Contêm código-fonte executável do aplicativo.
* Contêineres Docker - São instâncias ativas e em execução de imagens Docker.
* DockerHub - É o repositório público de imagens Docker.
* Daemon Docker - Serviço que cria e gerencia imagens Docker.

## Um pouco de prática com Docker

![Docker CLI](images/docker_cli.png){ width=100% }

[Comandos básicos do Docker](https://www.hostinger.com.br/tutoriais/docker-cheat-sheet?ppc_campaign=google_search_generic_hosting_all&bidkw=defaultkeyword&lo=20089&gad_source=1&gclid=Cj0KCQjwiMmwBhDmARIsABeQ7xRoIWDHydpFQs1Vx21fJLVALgokntMSDEdY0uTmg-XwY14u--r-yCUaAqmXEALw_wcB)

# Conceitos de AWS

## Amazon Web Services (AWS)
A Amazon Web Services (AWS) é a plataforma de nuvem mais adotada e mais abrangente do mundo, oferecendo mais de 200 serviços completos de datacenters em todo o mundo.

![AWS](images/amazon-aws.png){ width=15% }

## Infraestrutura Global da AWS
![regiões da AWS](images/aws-infra.png){ width=100% }

## Serviços 
![serviços](images/aws-services.png){ width=100% }

## Hora de conhecer a console da AWS!

## (curiosidade) AWS Snowball
![snowball](images/aws-snowball.jpg){ width=65% }

## (curiosidade) AWS SnowMobile
![snowmobile](images/aws-snowmobile.jpg){ width=70% }

# Gitlab 
O GitLab é um gerenciador de repositório de software baseado em git, com suporte a Wiki, gerenciamento de tarefas e CI/CD.

# Gitlab CI/CD
Os pipelines são escritos em um arquivo YAML na raiz do projeto.

```yaml
# .gitlab-ci.yml
stages:
  - test
  - build

fmt_code:
  stage: test
  needs: []
  image: golang:1.22.1
  rules:
    - if: $CI_COMMIT_BRANCH == "feature/add_pipeline"
  script:
    - cd hello-world/
    - go fmt

```

## Gitlab Runner
Aplicação que atua em conjunto com o Gitlab CI/CD para executar os jobs de um pipeline.   
Podem ser de duas formas:

- Hospedados pelo Gitlab 
- Auto hospedados/gerenciados

## Pipelines
Componente de alto nível que descreve o fluxo de CI/CD.   
São compostos por *Jobs*, que definem o que fazer e por *Stages* que definem quando executar os Jobs.

Pipelines normalmente são compostos por 4 Stages:

- *build*
- *test*
- *staging*
- *production*

## Variáveis
Variáveis no contexto de CI/CD são um tipo de variável de ambiente.
Você pode usar variáveis para:   

- Controlar o comportamento dos *Jobs* e Pipelines
- Salvar valores que serão reutilizados
- Evitar adicionar valores diretamente no arquivo `.gitlab-ci.yml`

## Jobs
Componente fundamental de um pipeline.

```yaml
meu-job:
  script: "cat /etc/os-release"
```

# Hora de colocar a mão na massa!
[https://gitlab.com/workshop-secs-2024/workshop-app](https://gitlab.com/workshop-secs-2024/workshop-app)


# Outras ferramentas de mercado para pipelines de CI/CD
:::incremental

- GitHub Actions
- Jenkins
- Circle CI
- Azure Pipelines

:::

# Outras ferramentas de mercado para hospedagem de aplicações
:::incremental

- AWS Amplify
- Vercel
- Heroku

:::

# Perguntas?

# Outros links para referências
- [Repositório do workshop](https://gitlab.com/workshop-secs-2024/workshop-app)
- [Plataforma de cursos da AWS](https://skillbuilder.aws/)
- [Repo no GitHub com mais recomendações sobre pipelines](https://github.com/cicdops/awesome-ciandcd)


# Obrigado pela atenção!
https://secs2024.edsoncelio.dev